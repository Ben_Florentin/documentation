<!DOCTYPE html>
<html>
<title>[IDT]</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<xmp theme="simplex" style="display:none;">

Dans ce tuto, il n'est expliquer que les fonctionnalités différentes ou nouvelles par rapport à Designer.
La quasi-totalité des fonctionnalités de Designer sont utilisables pour IDT.

(voir [designer.md])

# Qu'est ce que c'est ?

IDT (Information Design Tool) est la version plus récente de l'outil Designer ** voir [designer.md] **


# Modifier le format d'affichage d'une dimension

* clic droit sur la dimension
 * `Modifier le format d'affichage`
  * sélectionner `Données` et sélectionner le format désiré

# Fédération des données

* ** La couche de fédération est seulement disponible pour les fondations des données multi-sources (DFX). Elle permet de créer des tables férérées pouvant inclure des données provenant de toutes connexions **

Contexte d'exemple : nous avons 3 bases SQL Server différentes

* créer un nouveau projet (ici "BDD_Federation1")
* créer les connexions et la fondation de données
* dans la fondation de données
 * sélectionner `Courche de fédération`
 * il suffit de créer une table fédérée et de créer autant de `mapping` que besoin.
 * intégrer la table fédérée et supprimer les autres tables
 * enregistrer la fondation des données
 * créer une couche de gestion
 * clic droit sur le projet (`.blx`)
  * `Publier`
   * `Dans un référentiel`

# Utiliser une liste de valeurs

## Qu'est-ce qu'un paramètre ?

C'est une invite de données (prompt) renseigné directement dans l'univers
La valeur sélectionnée influera la donnnée concernée par la dimension où le paramètre est renseigné.

## Utiliser une liste de valeurs

* depuis l'onglet `Paramètres et listes de valeurs`
 * sélectionner l'icône de création d'une liste de valeurs
  * `Liste de valeurs basées sur des objets de la couche de gestion`
   * renseigner les informations propres à la liste de valeurs (nom, description)
   * `Modifier la requête`
    * sélectionner la dimension dont on souhaite récupérer les valeurs


* sélectionner l'i^cone de création d'un paramètre
 * renseigner les informations propres au paramètre (nom, description)
 * sélectionner la liste de valeurs créée depuis l'onglet `Liste de valeurs`
  * sélectionner `Liste de valeurs définie dans la couche de gestion` puis sélectionner la liste de valeurs


* créer une nouvelle dimension et associer le paramètre/la liste de valeurs

# Créer une invite de données en cascade
 
* créer une liste de valeurs
 * sélectionner l'icône de création d'une nouvelle liste de valeur dans l'onglet `Paramètres et listes de valeurs`
 * sélectionner l'option `Liste de valeurs basée sur des objets de la couche de gestion`
  * sélectionner le(s) champ(s) dont les valeurs seront issues


* sélectionner la dimension qui se basera sur la liste de valeurs
 * `Avancé`
  * sélectionner la liste de valeurs créée dans l'onglet `Liste de valeurs`


* clic droit sur la dimension paramétrée
 * `Aperçu de la liste des valeurs`

# Créer un univers multi-sources

* créer plusieurs connexions
* clic droit sur chacunes des connexions
 * `Publier la connexion dans un référentiel`
 * créer un raccourco pour chacune de ces connexions


* clic droit sur l'univers
 * `Nouveau`
  * `Fondation de données`
   * définir un nom et une description
   * sélectionner `Multi-sources activé`
   * se connecter au référentiel pour aller récupérer les connexions sécurisées
   * sélectionner les connexions créées précèdemment
   * on peut modifier la couleur de chacunes de ces connexions pour pouvoir les différencier

# Créer des dimensions temporelles dans la fondation de données

Contexte d'exemple : nous avons un champs `Date` reconnu comme `TIMESTAMP`, nous souhaitons avoir une dimension `Mois` et `Trimestre`

* aprés avoir réaliser la connexion à la source de données
 * se rendre dans la partie `Fondation de données`
  * clic droit sur le champs `Date`
   * `Insérer une colonne de temps`
    * `Mois`/ `Trimestre`


* sauvegarder la fondation des données (en gaut à droite)

* utiliser ces dimensions
 * depuis la partie `Couche de gestion` du projet
  * créer une nouvelle dimension
  * modifier la requête pour y sélectionner le champ `Mois` / `Trimestre`
  * vérifier l'intégrité de cette dimension en effectuant une requête dessus

# Créer un filtre prédéfini dynamique sur une date

* depuis `Couche de gestion` du projet
 * clic droit
  * `Nouveau`
   * `Filtre`
    * paramétrer ce filtre (ici on veut récupérer l'année actuelle)
    * accéder aux propriétés du flitres
    * modifier le code SQL (ici `TABLE."CHAMP" = convert (SMALLDATETIME, {fn CURDATE()})`)

# Créer un profil de sécurité de données

## Qu'est-ce qu'un profil de sécurité de données ?

C'est un groupe de paramètres qui définit la sécurité sur un univers publié à l'aide d'objets de la fondation de données et de connexions de données
Il permet entre autre, de resteindre l'accès et/ou l'utilisation de certaines données dans votre univers pour certains groupes d'utilisateurs

** Tous les profils de sécurité s'appliquent uniquement aux univers relationnels ** 

Contexte d'exemple : on souhaite que les utilisateurs d'un groupe ne puissent utiliser que les valeurs de l'année X

* depuis le volet `Univers/Profils de l'éditeur de sécurité`
 * sélectionner l'univers concerné
 * pour modifier un profil existant
  * clic droit sur le profil
   * `Modifiez le profil de sécurité des données`
 * pour insérer un profil
  * clic droit sur le nom de l'univers
   * `Insérer un profil de sécurité des données`
 * depuis l'onglet `Lignes`
  * `Insérer`
   * sélectionner la table qui contient le champ que l'on souhaite filtrer (ici `Année`)
   * défnir la clause `Where` (ici `SALE."Année_SALE_DATE" = 2005`)


* depuis l'onglet `Univers/Profils`
 * sélectionner le profil crée précèdemment
 * transférer les groupes d'utilisateurs que l'on souhaite qu'ils utilisent ce profil


* sauvegarde des modifications
* clic droit sur l'univers
 * `Publier dans un référentiel`
 
</xmp>
<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
</html>