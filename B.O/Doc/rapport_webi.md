<!DOCTYPE html>
<html>
<title>[rapport_webi]</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<xmp theme="simplex" style="display:none;">

# Effectuer un filtre sur une section

* clic droit sur la colonne d'un tableau
 * `Définir comme section`

# Transformer un tableau en diagramme

* clic droit sur le tableau
 * `Transformer en...`
  * sélectionner le type du diagramme

# Définir un contrôle d'entrée

**Un contrôle d'entrée permet d'effectuer un filtre sur des données dans un rapport**

* clic droit sur un tableau
 * `Mise en relation`
  * `Ajoutez un lien d'élément`
  * sélectionner l'objet cible
  * définir les propriétés de contrôle
  * sélectionner les éléments du rapport qui prendront en compte le filtre **(tableaux/graphiques,etc..)**

# Créer un contrôle d'entrée groupé (en cascade)

* clic sur l'icone d'un contrôle d'entrée -> `Nouveau`
 * sélectionner la dimension
 * sélectionner/paramétrer le type de contrôle
 * sélectionner les élements du rapport qui seront affectés


* réitérer ces étapes pour chaque contrôle d'entrée
* clic sur l'icône du contrôle d'entrée -> `Groupe`
 * nommer le nom du groupe
 * sélectionner les contrôles d'entrées à grouper

# Paramétrer un digramme

* clic droit sur un diagramme
 * `Mettre en forme`

# Propager un filtre dans d'autres rapports

* créer un contrôle d'entrée ** (voir [rapport_webi.md->Défnir un contrôle d'entrée]) **
 * sélectionner une variable qui va permettre d'effectuer un filtre
 * sélectionner le type de contrôle


* créer une variable permettant d'afficher uniquement le résultat filtré
 * clic droit sur `variables`
 * `Nouvelle variable`
 * définir la formule
  * **ex: `=si [sales revenue] > [sale] alors [Store name] sinon "autres"`**
 * modifer/créer un tableau/graphique
 * sélectionner la variable permettant d'afficher le résultat filtré


* modifier les données à afficher dans le graphique du 2nd rapport
 * clic droit sur le diagramme
  * `Affecter les données`
  * ** Ne pas modifier la variable permettant d'afficher les données filtrées !!!**
  * on peut modifier les autres données comme bon nous semble

# Fusion de dimensions (merge dimensions)

depuis le **Data provider** (interface de gestion des requêtes)

* `Ajouter une requête`
 * sélectioner l'option que l'on désire
 * suivre les instructions puis valider


* choisir les **dimensions/préfiltres/informations/indicateurs** dans les différentes requêtes
* `Exécutez les requêtes`

depuis le **rapport WEBI**

* `Accès aux données`
 * `fusionner`
  * sélectionner les **dimensions** à fusionner

# Fonction Max/Min/Pourcentage

* sélectionner une cellule de la colonne à calculer
* `Analyse`
 * `Fonctions`
  * `Autres`
   * `Max` / `Min` / `Pourcentage`

une nouvelle ligne est alors ajoutée à la fin du tableau affichant la valeur recherchée

# Créer un élément partagé

**Déf : élément accessible à plusieurs utilisateurs différents.**

* sélectionner un objet (tableau/graph) **ATTENTION : SELECTIONNER L'OBJET EN ENTIER ! PAS UNE CELLULE OU AUTRE !!**
* clic droit
 * `Mise en relation`
 * `Enregistrer en tant qu'élément partagé`
 * choisir le dossier de sauvegarde
  * **IMPORTANT : DOIT ÊTRE DANS UN DOSSIER PUBLICS**

depuis le rapport WEBI, connecté avec un autre utilisateur **ayant les droits d'accès au dossier où se situe l'élément partagé** :

* selectionner l'icône de partage
 * `Parcourir`
 * glisser/déposer l'élément partagé dans le rapport WEBI

# Renvoi à la ligne

* clic droit sur la cellule d'une colonne
 * `Texte`
  * `Renvoi à la ligne automatique`

# Gestion de l'affichage du rapport (mode Plan/mode "Replier/Déplier")

* `Analyse`
 * `Intéragir`
  * `Plan`

Il est maintenant possible de **déplier/replier** les **différents objets** du rapport grâce aux **flêches** situées sur le **côté gauche** et **au dessus** du rapport

# Effectuer un classement Top/Flop

* sélectionner les données dans un graphique (1 indicateur minimum)
* clic droit
 * `Classement`
  * `Ajouter le classement`

définir les points suivants :

* valeur à afficher : plus élevées/plus basses
* indicateur à utiliser
* contexte de calcul (`classé par`)
 * Si le tableau est composé de plusieurs dimensions, par défaut le contexte de calcul pour définir les plus hautes/basses valeurs tient compte de toutes ces dimensions.
 * Si le classement ne doit porter que sur une dimension en particulier, il faut le préciser
* mode de calcul

# Insérer un commentaire

* `Elements du rapports`
 * `Commentaire`
  * `Insérer une cellule de commentaire`

## Répondre à un commentaire

* sélectionner le commentaire
* `Elements du rapport`
 * `Commentaire`
  * `Insérer une cellule de commentaire`

# Cartes géographiques

* clic droit sur une dimension
 * `Modification en tant que géographie`
   * `Par nom`


* indiquer le niveau de zone géographique
* sélectionner la dimension géographique avec au moins un indicateurs
* créer un diagramme géographique (clic sur l'icône géographique)

# Mettre en forme une chaîne de caractères précise

* clic droit sur la cellule cible
 * `Mettre en forme`
  * `Général`
   * `Lire le contenu de la cellule comme`
    * mettre en `HTML`


* modifier la formule de la cellule/variable afin d'insérer les balises souhaitées

# Fonction Abs()

* créer une variable en tant qu'**indicateur**
* saisir la formule `=Abs(**parametre**)`
* ajouter cette variable au graphique/tableau souhaité

# Prompt (invite de commande)

* créer une cellule vide
* modifier la formule : `=RéponseUtilisateur()`

**Cette fonction est disponible dans le dossier `Fournisseur de données` et `Toutes`**

toujours dans l'interface de modification de formule:
 * sélectionner l'opérateur `Invites`
 * sélectionner l'invite pour laquelle on veut récupérer la réponse
  * **ex : `=RéponseUtilisateur("Saisir une ou plusieurs valeurs pour Year")`**

** ATTENTION !! La fonction `RéponseInvite()` renvoi un booléen. Il est possible de créer directement une cellule contenant la réponse à l'invite. Le résultat est identique **
* sélectionner une cellule
* `Elements du rapport`
 * `Cellule`
  * `Prédéfini`
   * `Invite`
    * sélectionner l'invite

# Afficher/personnaliser les étiquettes d'un graphique

* clic droit sur le diagramme
 * `Mettre en forme le diagramme`
  * `Global`
   * `Palette et style`
     * `Présentation 3D` : si c'est un diagramme à colonne
     * `3D` : si diagramme à secteur

** si c'est un diagramme à secteur, il est possible de régler la profondeur

# Mise en relation - Lien d'élément

**Contexte d'exemple : dans notre exemple, on a 2 tableaux. Le but est d'effectuer un clic sur les années situées dans un tableaux afin que le tableau contenant les mois et le total des ventes s'adapte à l'année choisie.**

* clic droit sur une année
 * `Mise en relation`
  * `Ajouter un lien d'élément`
   * définir un contrôle d'entrée
    * donnée servant de filtre **(ici, l'année)**
    * nom du contrôle d'entrée **(ici, `année`)**
    * objet cible **(ici, tableau 2)**

# Position relative d'un élément

**Contexte d'exemple : on a un titre et 2 tableaux. Le but est d'espacer les tableaux de X cm entre eux et de X cm avec le titre**

* clic droit sur le titre
 * `Alignement`
  * `Renvoi à la ligne automatique`

**SOIT**

* clic droit sur le 1er tableau 
 * `Aligner`
  * `Position relative`

**OU**

* clic droit sur le 1er tableau
 * `Format du tableau`
  * `Présentation`
   * `Position relative verticale`
    * X cm
    * `Bord inférieur` au `Titre du rapport`



* clic droit sur le 2nd tableau
 * `Format du tableau`
  * `Présentation`
   * `Position relative horizontale`
    * X cm
    * `Bord droit` au `1er tableau`
   * `Position relative verticale`
    * 0 cm **(alignement vertical avec le 1er tableau)**
    * `Bord supérieur`
    * `1er tableau`

# Figer les colonnes/lignes d'un tableau

en mode lecture

* sélectionner une cellule/ligne/colonne
* `Figer`
 * choisir l'option désirée

# Calculer la différence en "%" entre 2 colonnes numériques d'un tableau

* clic droit sur la dernière colonne
 * `Insérer`
  * `Insérer une colonne à droite`


* nommer la nouvelle colonne
* modifier la formule de la seconde cellule de la nouvelle colonne
 * `choisir un indicateur divisé par un second indicateur -1`
 * **ex: `"= [2014] / [2013] - 1"`**


* clic droit sur la même cellule
 * `Format de nombre`
  * `Pourcentage`
   * `123 456,70 %`

# Grouper les valeurs

* sélectionner une colonne d'un tableau
* `Analyse`
 * `Affichage`
  * `Grouper`
   * `Créer des groupes`
    * nommer le groupe
    * sélectionner les valeurs à grouper

# Fonction Nombre

* sélectionner une cellule de la colonne
* `Analyse` 
 * `Fonction`
  * `Nombre`

# Mise en forme conditionnelle

* **La mise en forme conditionnelle peut s'appliquer sur les éléments suivants :**
 * **colonnes d'un tableau vertical**
 * **lignes d'un tableau horizontal**
 * **cellule de formulaires/tableau croisés**
 * **entête de section**
 * **cellules individuelles**


* sélectionner l'un des élements énumérés précèdemment
* `Analyse`
 * `Conditionnelle`
  * `Nouvelle règle`
   * `nom` : nom de la règle
   * `objet ou cellule filtrés` : objet/cellule concerné
   * `opérateur` : condition
   * `opérande` : valeur de la condition
   * `Mettre en forme`
    * on peut modifier l'affichage de la donnée

# Activer le suivi des données

* `Analyse`
 * `Suivi des données`
  * `suivre`
   * `Comparer avec la derniere actualisation des données` : les données actuelles deviennent les données de référence après chaque actualisation de données. Affiche toujours la différence entre les données les plus récentes et les données précédentes
   * `Comparer avec l'actualisation des données de` : les données à la date de l'actualisation choisie deviennet les données de référence. Le rapport affiche la même chose que l'option précédente

# Créer un document avec exploration

**Contexte d'exemple : le but est de créer un docuement intéractif qui permet d'explorer les données (avant/arrière)**

* `Accès aux données`
 * `Modifier`
  * clic sur le panneau périmètre d'analyse


* dans `Périmètre d'analyse`
 * glisser/déposer les dimensions que l'on souhaite explorer


* `Exécutez la requête`


* maintenant, quand on point le curseur sur les données du tableau, il nous est proposé : `Explorer en avant`
 * au clic -> les données changeront en fonction du niveau de l'exploration

# Créer un lien hypertexte

* créer une cellule qui contiendra le lien hypertexte
* clic droit dessus
 * `Mise en relation`
  * `Ajouter un lien hypertexte`
   * saisir l'adresse du lien
   * `Analyser`
   * dans `fenêtre cible`, choisir l'option puis valider

# Fonction FormatNumber

* syntaxe : `=FormatNumber(nombre;format_chaine)`

* avant : `500 205,68 €`

* requête : `=FormatNumber([Montant de l'opération]; "# ### €")`

* resultat : `500 205 €`

# Fonction FormatDate

* syntaxe : `=FormatDate(date;format_chaine)`

* avant : `01/08/73`

* requête : `=FormatDate([Date]; "dd mmm yyyy")`

* resultat : `01 août 1973`

# Fonction Droite

* Objectif : renvoie les caractères les plus à droite d'une chaîne

* syntaxe : `=Droite(chaîne;nb_caractères)`

* avant : `01010 Location conteneur`

* requête : `=Droite([Lib]; 18)`

* resultat : `Location conteneur`

# Fonction Concaténation

* syntaxe : `=Concaténation(chaîne_1;chaîne_2)`

* avant : `Abo cable` et `07200 Redevance`

* requête : `=Concaténation("Abo cable" + " "; "07200 Redevance")`

* resultat : `Abo cable 07200 Redevance`

</xmp>
<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
</html>