<!DOCTYPE html>
<html>
<title>[presentation_bo]</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<xmp theme="simplex" style="display:none;">

# Présentation B.O

B.O utilise une couche sémantique, appelée `univers` qui permet de structurer les données issues du base de données (Data Warehouse, etc...) et de les afficher.

Les utilisateurs ont accès uniquement aux données affichées depuis la couche sémantique.


## Objet et Classe d'objet

Le plus souvent un objet est un champs du table renommé

Plusieurs types d'objet:

* `dimension` : objet sur lequel on peut faire des opérations ou que l'on peut afficher dans les tableaux/les diagrammes
* `information` : attribut supplémentaire à un objet de type `dimension`. En général, on ne fait pas de filtre/opération sur les objets de type informations
* `filtre` : objet non affiché servant à intérroger les données. Ils sont prédèfinis par les developpeurs pour les utilisateurs.
* `indicateur` : objet dynamique contenant une règle de gestion. Ces objets agissent diffèrement en fonction de l'objet parent.

** Exemple de hierarchie **

	- Magasin <- Classe
		- Etat  <- Dimension 
		- Ville  <- Dimension 
			- CA  <- Indicateur 
		- Nom du Magasin  <- Dimension 
		- Taille de la surface de vente ?  <- Filtre 
		- Magasin avec large horaire des ouvertures  <- Filtre 
		- Détails du Magasin  <- Sous classe 

Dans l'exemple ci-dessus, l'indicateur `CA` va permettre d'obtenir le CA par Ville de toutes les Villes

Si l'on veut obtenir le CA par Magasin, il faut alors mettre l'indicateur dans l'objet `Nom du Magasin` **(voir l'exemple ci-dessous)**

	- Magasin  <- Classe 
		- Etat  <- Dimension 
		- Ville  <- Dimension 
		- Nom du Magasin  <- Dimension 
			- CA  <- Indicateur 
		- Taille de la surface de vente ?  <- Filtre 
		- Magasin avec large horaire des ouvertures  <- Filtre 
		- Détails du Magasin  <- Sous classe 

## Liste de valeurs

Liste contenant les valeurs créées par le développeur. Ces valeurs permettront d'être selectionnées par l'utilisateur pour les filtres

## Document

Classeur contenant plusieurs rapports permettant de construire des tableaux/graphiques etc... Il est utilisé par les utilisateurs.

## Rapport

Fichier contenant des **blocks** et **cellules** (tableaux/graphs). Permet d'afficher les données

## Référentiel (CMC)

Environnement permettant à l'administrateur de configurer les droits aux utilisateurs sur l'accès aux documents, rapports, données.

## Vocabulaire technique

* `Data provider` : `query`
* `input parameters` : `filtre dynamique`
* `prompt`: `invite de commande`
* `input control` : `contrôle d'entrée`

</xmp>
<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
</html>