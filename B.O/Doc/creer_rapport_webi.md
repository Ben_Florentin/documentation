<!DOCTYPE html>
<html>
<title>[creer_rapport_webi]</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<xmp theme="simplex" style="display:none;">

# Créer un rapport WEBI

* `Applications`
 * `Applications WEB Intelligence`
  * sélectionner un univers

* Sélectionner les objets que l'on souhaite interroger **(Plan de l'univers)**
* Glisser/déposer les dans la partie **Objet du résultat**
* `Exécutez la requête`

## Visualisation du contenu de l'univers

* `Plan de l'univers` ** (à gauche) ** : architecture du contenu de l'univers 
* `Objet du résultat` ** (à droite, en haut) ** : objet(s) sélectionné(s) pour être affiché dans le rapport
* `Filtres de la requête` ** (à droite, au milieu) ** : filtre(s) prédéfinis par le développeur afin de pouvoir trier/filtrer les données avant le paramètrage des données dans le rapport WEBI
* `Aperçu des données` ** (à droite, en bas) ** : objet(s) contenu(s) dans le rapport WEBI

## Rapport WEBI

* `objets disponibles` ** (à gauche) ** : architecture des objets contenus dans le rapport WEBI
* ** (à droite) ** : rapport WEBI

</xmp>
<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
</html>