<!DOCTYPE html>
<html>
<title>[designer]</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<xmp theme="simplex" style="display:none;">

# Qu'est ce que c'est ?

Designer est un outil de référence pour la création d'application de tableau de bord au format HTML5.

Il permet d'agir au niveau de la Modélisation Conceptuelle des Données (MCD) : c-a-d la relation entre les tables d'une base de données, les jointures, etc...

# Insérer une table

* clic droit dans le carré blanc de droite
 * `Tables..`
  * double clic sur toutes les tables nécessaires


* clic sur l'icône en forme d'échelle
 * `Arrange Tables` : permet d'afficher les tables correctement

# Jointure

* gilsser/déposer un champs d'une table vers le champs d'une autre table
* clic droit sur la jointure
 * `Join properties...`
  * paramétrer la jointure (cardinality, and so on...) or `detect`

# Contextes
 
* `Tools`
 * `Automated detection`
  * `Detect contexts...`
   * `add` : ajout des contextes

# Gestion des erreurs

* `Tools`
 * `Check Integrity`
  * sélectionner l'option désirée

# Création d'objets

Pour créer des objets, il faut SAUVEGARDER l'univers ensuite on peut continuer.

* `Insert`
 * `Class..`
  * nommer/typer l'objet
  * ajouter une description
  * `Select` : détermine la source de données
  * `Where` : détermine la condition
  * `Propriétés` : définit le type de l'objet (dimension/information/indicateur)

# Création de filtres

* au niveau de l'architecture d'objets (à gauche), en bas il y a une liste d'icônes
 * sélectionner l'icône `filtre`
 * clic droit sur un objet dans l'architecture
  * `Condition...`
   * définir nom, description ainsi que la condition !
    * ** ex : `"ucase(Calendar_year_lookup.Holiday_Flag) = 'Y'"` **


</xmp>
<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
</html>